<?php

use App\Models\Department;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('departments', \App\Http\Controllers\DepartmentController::class);
Route::resource('employees', \App\Http\Controllers\EmployeeController::class);

Route::get('/test', function () {
    $avg = Department::query()->with('depHasMany2')->first();
//    $d->increment('count_of_employees');
    dd($avg);
});
Route::get('/2', function () {
//    function generate_code($length)
//    {
//        $chars = 'qwertyuiopasdfghjklzxcvbnm1234567890QQWERTYUIOPASDFGHKLZXCVBNM';
//        $numChars = strlen($chars); // Узнаем, сколько у нас задано символов
//        $str = '';
//        for ($i = 0; $i < $length; $i++) {
//            $str .= substr($chars, rand(1, $numChars) - 1, 1);
//        } // Генерируем код
//
//        // Перемешиваем, на всякий случай
//        $array_mix = preg_split('//', $str, -1, PREG_SPLIT_NO_EMPTY);
//        srand((float)microtime() * 1000000);
//        shuffle($array_mix);
//        // Возвращаем полученный код
//        return implode("", $array_mix);
//    }
//    dd(generate_code(6));
//    $timestamp = strtotime('20.05.2022');
//
//    $day = date('D', $timestamp);
//    var_dump($day);

    function findStringDate($string)
    {
        $week = ['Sun' => 'вс', 'Mon' => 'пн', 'Tue' => 'вт', 'Wed' => 'ср', 'Thu' => 'чт', 'Fri' => 'пт', 'Sat' => 'сб'];

        $pattern = "/\d{2}\.\d{2}\.\d{4}/";
        if (preg_match_all($pattern, $string, $matches)) {
            foreach (array_unique($matches[0]) as $value) {
                $timestamp = strtotime($value);
                $day = $week[date('D', $timestamp)];

                $string = str_replace($value, $value . ' (' . $day . ')', $string);
            }
        }
        return $string;
    }

    var_dump(findStringDate('Давайте устроим встречу 20.05.2022 и потом ещё одну 12.06.2022'));

});
