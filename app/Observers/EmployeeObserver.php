<?php

namespace App\Observers;

use App\Models\Employee;

class EmployeeObserver
{
    public function saved(Employee $employee)
    {
        foreach ($employee->dep_emp as $item) {
            $item->update([
                'avg__salary' => $item->emp_dep->avg('salary'),
                'count_of_employees' => $item->emp_dep->count(),
            ]);
        }
    }
}
