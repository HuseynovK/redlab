<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'surname' => $this->surname,
            'father_name' => $this->father_name,
            'salary' => $this->salary,
            'gender' => $this->gender == 1 ? 'Male' : 'Female',
            'deps' => $this->dep_emp ? $this->dep_emp->pluck('name')->implode(',') : [],
            'deps2' => $this->dep_emp,
        ];
    }
}
