<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:255', 'string'],
            'surname' => ['required', 'max:255', 'string'],
            'father_name' => ['required', 'max:255', 'string'],
            'gender' => ['required', 'integer', 'in:1,2'],
            'salary' => ['required', 'integer'],
            'dep.*' => ['required', 'exists:departments,id'],
        ];
    }
}
