<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $data = Employee::with('dep_emp')->get();
//        dd($data);
        return view('welcome',compact('data'));
    }
}
