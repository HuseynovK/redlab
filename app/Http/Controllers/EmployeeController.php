<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Http\Resources\EmployeeResource;
use App\Http\Resources\EmpResource;
use App\Models\Employee;
use App\Services\DepartmentService;
use App\Services\EmployeeService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class EmployeeController extends Controller
{
    private DepartmentService $departmentService;
    private EmployeeService $employeeService;

    public function __construct(DepartmentService $departmentService, EmployeeService $employeeService)
    {
        $this->departmentService = $departmentService;
        $this->employeeService = $employeeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $dep = $this->employeeService->getAll(true);
        return view('pages.employee.list', compact('dep'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $dep = $this->departmentService->getAll();
        return view('pages.employee.create', compact('dep'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $result = $this->employeeService->createDep($request->validated());
        if (!$result) {
            return Response::fail(['fail' => ['Something went wrong']], 'Failed');
        }
        return Response::success('Successful', route('employees.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Employee $employee)
    {
        $dep = $this->departmentService->getAll();
        $employee = $this->employeeService->getDep($employee);
        return view('pages.employee.update', compact('employee', 'dep'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, Employee $employee)
    {
        $result = $this->employeeService->updateEmp($employee, $request->validated());
        if (!$result) {
            return Response::fail(['fail' => ['Something went wrong']], 'Failed');
        }
        return Response::success('Successful', route('employees.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Employee $employee
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Employee $employee)
    {
        $this->employeeService->deleteEmp($employee);
        return redirect()->back();
    }
}
