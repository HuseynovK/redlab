<?php


namespace App\Services;

use App\Http\Resources\DepartmentResource;
use App\Repository\Contactor\DepartmentInterface;

class DepartmentService
{
    private DepartmentInterface $departmentRepository;

    public function __construct(DepartmentInterface $departmentRepository)
    {
        $this->departmentRepository = $departmentRepository;
    }

    public function getAll($paginate = false)
    {
        return $this->departmentRepository->index($paginate);
    }

    public function createDep($data)
    {
        return $this->departmentRepository->create($data);
    }

    public function getDep($data)
    {
        return (new DepartmentResource($data));
    }

    public function updateDep($model, $data)
    {
        return $model->update($data);
    }

    public function deleteDep($model)
    {
        return $this->departmentRepository->delete($model);
    }
}
