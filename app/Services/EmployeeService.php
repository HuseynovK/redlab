<?php


namespace App\Services;

use App\Http\Resources\DepartmentResource;
use App\Http\Resources\EmployeeResource;
use App\Repository\Contactor\DepartmentInterface;
use App\Repository\Contactor\EmployeeInterface;

class EmployeeService
{
    private DepartmentInterface $departmentRepository;
    private EmployeeInterface $employeeRepository;

    public function __construct(DepartmentInterface $departmentRepository, EmployeeInterface $employeeRepository)
    {
        $this->departmentRepository = $departmentRepository;
        $this->employeeRepository = $employeeRepository;
    }

    public function getAll($paginate = false)
    {
        return $this->employeeRepository->index($paginate);
    }

    public function createDep($data)
    {
        return $this->employeeRepository->create($data);
    }

    public function getDep($data)
    {
        return (new EmployeeResource($data));
    }

    public function updateEmp($model, $data)
    {
        return $this->employeeRepository->update($model, $data);
    }

    public function deleteEmp($model)
    {
        return $this->employeeRepository->delete($model);
    }
}
