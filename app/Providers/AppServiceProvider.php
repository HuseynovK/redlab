<?php

namespace App\Providers;

use App\Mixin\ResponceMixin;
use App\Models\Employee;
use App\Observers\EmployeeObserver;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        Response::macro('success', function ($message, $url) {
//            return ['success' => true, 'message' => $message, 'url' => $url];
//        });
//
//        Response::macro('fail', function ($m, $message) {
//            return ['errors' => $m, 'message' => $message];
//        });
        Response::mixin(new ResponceMixin());
        Employee::observe(EmployeeObserver::class);
    }
}
