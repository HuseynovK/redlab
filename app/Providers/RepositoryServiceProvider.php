<?php

namespace App\Providers;

use App\Repository\Contactor\DepartmentInterface;
use App\Repository\Contactor\EmployeeInterface;
use App\Repository\DepartmentRepository;
use App\Repository\EmployeeRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(DepartmentInterface::class, DepartmentRepository::class);
        $this->app->singleton(EmployeeInterface::class, EmployeeRepository::class);
    }
}
