<?php


namespace App\Repository\Contactor;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface CrudInterface
{
    public function index($paginate = false);

    public function create(array $data);

    public function edit(Model $model);

    public function update(Model $model, $data);

    public function delete(Model $model);
}
