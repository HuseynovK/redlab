<?php


namespace App\Repository;


use App\Http\Resources\DepartmentResource;
use App\Http\Resources\EmployeeResource;
use App\Models\Department;
use App\Models\Employee;
use App\Repository\Contactor\DepartmentInterface;
use App\Repository\Contactor\EmployeeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmployeeRepository extends BaseRepository implements EmployeeInterface
{
    public function __construct(Employee $model)
    {
        parent::__construct($model);
    }

    public function index($paginate = false)
    {
        $res = $this->model->orderByDesc('created_at')->select('id', 'name', 'surname', 'father_name', 'salary', 'gender');
        if ($paginate) {
            $k = $res->with('dep_emp:id,name')->paginate(10);
            return EmployeeResource::collection($k);
        }
        return EmployeeResource::collection($res->get());
    }

    public function create(array $data)
    {
        DB::beginTransaction();
        $m = $this->model->create($data);
        $m->dep_emp()->attach($data['dep']);
        DB::commit();

        return $m;
    }

    public function edit(Model $model)
    {
        // TODO: Implement edit() method.
    }

    public function update(Model $model, $data)
    {
        DB::beginTransaction();
        $model->update($data);
        $model->dep_emp()->detach();
        $model->dep_emp()->attach($data['dep']);
        DB::commit();

        return true;
    }

    public function delete(Model $model)
    {
        $model->delete();
    }
}
