<?php


namespace App\Repository;


use App\Http\Resources\DepartmentResource;
use App\Models\Department;
use App\Repository\Contactor\DepartmentInterface;
use Illuminate\Database\Eloquent\Model;

class DepartmentRepository extends BaseRepository implements DepartmentInterface
{
    public function __construct(Department $model)
    {
        parent::__construct($model);
    }

    public function index($paginate = false)
    {
        $res = $this->model->orderByDesc('created_at')->select('id', 'name', 'avg__salary', 'count_of_employees');
        if ($paginate) {
            return DepartmentResource::collection($res->paginate(5));
        }
        return DepartmentResource::collection($res->get());
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function edit(Model $model)
    {
        // TODO: Implement edit() method.
    }

    public function update(Model $model, $data)
    {
        // TODO: Implement update() method.
    }

    public function delete(Model $model)
    {
        $rel = $model->emp_dep->isEmpty();
//        dd($model->emp_dep);
        if ($rel) {
            $model->delete();
            return true;
        }
        return false;
    }
}
