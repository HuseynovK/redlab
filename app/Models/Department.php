<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();
    }

    public function emp_dep()
    {
        return $this->belongsToMany(Employee::class, 'employee_departments');
    }

    public function depHasMany()
    {
        return $this->hasMany(EmployeeDepartment::class, 'department_id');
    }

    public function depHasMany2()
    {
        return $this->hasManyThrough(EmployeeDepartment::class, Employee::class, 'id', 'employee_id', 'id', 'id');
    }
}
