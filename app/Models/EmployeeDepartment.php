<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeDepartment extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function dep()
    {
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function emp()
    {
        return $this->belongsToMany(Employee::class, 'employee_id');
    }
}
