<?php

namespace App\Models;

use App\Observers\EmployeeObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Employee extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($emp) {
            Log::info("Entereddddddddddddddddddddddddddddddddddddddddd");
            foreach ($emp->dep_emp as $item) {
                $item->update([
                    'avg__salary' => $item->emp_dep->avg('salary'),
                    'count_of_employees' => $item->emp_dep->count(),
                ]);
            }
        });
//        self::observe(EmployeeObserver::class);
    }

    public function dep_emp()
    {
        return $this->belongsToMany(Department::class, 'employee_departments');
    }

    public function depHasMany()
    {
        return $this->hasMany(EmployeeDepartment::class, 'employee_id');
    }

    public function depHasMany2()
    {
        return $this->hasManyThrough(EmployeeDepartment::class, Department::class, 'id', 'department_id', 'id', 'id');
    }
}
