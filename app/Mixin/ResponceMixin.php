<?php


namespace App\Mixin;


use Illuminate\Http\Response;

class ResponceMixin
{
    public function success()
    {
//        Response::macro('success', function ($message, $url) {
//            return ['success' => true, 'message' => $message, 'url' => $url];
//        });
//
//        Response::macro('fail', function ($m, $message) {
//            return ['error' => $m, 'message' => $message];
//        });
        return function ($message, $url) {
            return ['success' => true, 'message' => $message, 'url' => $url];
        };
    }

    public function fail()
    {
        return function ($m, $message) {
            return ['error' => $m, 'message' => $message];
        };
    }
}
