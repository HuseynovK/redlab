@extends('layouts.app')
@section('content')
    <main class="flex-shrink-0" style="padding-top: 70px">
        <div class="container">
            <h1>Department Create</h1>
            <div class="depc"></div>
            <form action="{{route('departments.store')}}" method="post" id="create_department">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Name</label>
                    <input type="text" class="form-control" name="name" value="{{old('name')}}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </main>
@endsection
