@extends('layouts.app')
@section('content')
    <main class="flex-shrink-0" style="padding-top: 70px">
        <div class="container">
            <a class="btn btn-primary" href="{{route('departments.create')}}">Add</a>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Number of employees</th>
                    <th scope="col">AVG salary among employees</th>
                </tr>
                </thead>
                <tbody>
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @foreach($dep as $d)
                    <tr>
                        <th scope="row">{!! $d->id !!}</th>
                        <td>{{$d->name}}</td>
                        <td>{{$d->count_of_employees}}</td>
                        <td>{{$d->avg__salary}}</td>
                        <td>
                            <form action="{{route('departments.destroy',['department'=>$d->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>
                            <a class="btn btn-success"
                               href="{{route('departments.edit',['department'=>$d->id])}}">Edit</a>
                        </td>
                    </tr>
                @endforeach
                {{$dep->links('pagination::bootstrap-4')}}
                </tbody>
            </table>
        </div>
    </main>
@endsection
