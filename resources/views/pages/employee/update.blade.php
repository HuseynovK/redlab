@extends('layouts.app')
@section('content')
    <main class="flex-shrink-0" style="padding-top: 70px">
        <div class="container">
            <h1>Employee Update</h1>
            <div class="depu"></div>
            <form action="{{route('employees.update',['employee'=>$employee->id])}}" method="post"
                  id="update_department">
                @csrf
                @method('put')
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Name</label>
                    <input type="text" class="form-control" name="name" value="{{old('name',$employee->name)}}">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Surname</label>
                    <input type="text" class="form-control" name="surname"
                           value="{{old('surname',$employee->surname)}}">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Father name</label>
                    <input type="text" class="form-control" name="father_name"
                           value="{{old('father_name',$employee->father_name)}}">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Gender</label>
                    <br>
                    Male
                    <input type="radio" name="gender" value="1"
                           @if(old('gender',$employee->gender) == 1) checked @endif>
                    Female
                    <input type="radio" name="gender" value="2"
                           @if(old('gender',$employee->gender) == 2) checked @endif>
                </div>
                <label for="salary">Salary</label>
                <input type="tel" name="salary" value="{{old('salary',$employee->salary)}}">
                <br>
                <br>
                <label for="deb">Departments</label>
                <br>
                @php
                    $v = $employee->dep_emp->pluck('id')->toArray();
                @endphp
                @foreach($dep as $d)
                    <label for="dep">
                        {{$d->name}}</label>
                    <input type="checkbox" name="dep[]" {{in_array($d->id,$v) ? 'checked' : ''}} value="{{$d->id}}"><br>
                @endforeach
                <br>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </main>
@endsection
