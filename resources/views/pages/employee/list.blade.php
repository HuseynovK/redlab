@extends('layouts.app')
@section('content')
    <main class="flex-shrink-0" style="padding-top: 70px">
        <div class="container">
            <a class="btn btn-primary" href="{{route('employees.create')}}">Add</a>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Surname</th>
                    <th scope="col">Father name</th>
                    <th scope="col">Gender</th>
                    <th scope="col">Salary</th>
                    <th scope="col">Departments</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($dep as $d)
                    <tr>
                        <th scope="row">{!! $d->id !!}</th>
                        <td>{{$d->name}}</td>
                        <td>{{$d->surname}}</td>
                        <td>{{$d->father_name}}</td>
                        <td>{{$d->gender ? 'Male' : 'Female'}}</td>
                        <td>{{$d->salary}}</td>
                        <td>{{ $d->dep_emp->pluck('name')->implode(',')}}</td>
                        <td>
                            <form action="{{route('employees.destroy',['employee'=>$d->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </form>
                            <a class="btn btn-success"
                               href="{{route('employees.edit',['employee'=>$d->id])}}">Edit</a>
                        </td>
                    </tr>
                @endforeach
                {{$dep->links('pagination::bootstrap-4')}}
                </tbody>
            </table>
        </div>
    </main>
@endsection
