@extends('layouts.app')
@section('content')
    <main class="flex-shrink-0" style="padding-top: 70px">
        <div class="container">
            <h1>Employee Create</h1>
            <div class="depc"></div>
            <form action="{{route('employees.store')}}" method="post" id="create_employee">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Name</label>
                    <input type="text" class="form-control" name="name" value="{{old('name')}}">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Surname</label>
                    <input type="text" class="form-control" name="surname" value="{{old('surname')}}">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Father name</label>
                    <input type="text" class="form-control" name="father_name" value="{{old('father_name')}}">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Gender</label>
                    <br>
                    Male
                    <input type="radio" name="gender" value="1" @if(old('gender',1) == 1) checked @endif>
                    Female
                    <input type="radio" name="gender" value="2" @if(old('gender') == 2) checked @endif>
                </div>
                <label for="salary">Salary</label>
                <input type="tel" name="salary" value="{{old('salary')}}">
                <br>
                <br>
                <label for="deb">Departments</label>
                <br>
                @foreach($dep as $d)
                    <label for="dep">
                        {{$d->name}}</label>
                    <input type="checkbox" name="dep[]" value="{{$d->id}}"><br>
                @endforeach
                <br>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </main>
@endsection
