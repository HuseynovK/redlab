$(document).ready(function () {
    $("#create_department").submit(function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        let form = $(this);
        let formData = new FormData(form[0]);
        fetch(form.attr('action'), {
            method: form.attr('method'),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'accept': 'application/json'
            },
            body: formData,
        })
            .then(response => response.json())
            .then(function (response) {
                console.log(response)
                if (response.success) {
                    $("body").removeClass("active");
                    window.location.href = response.url;
                } else {
                    $(".depc").html(' ');
                    if (response.errors != undefined) {
                        if (Object.keys(response.errors).length != 0) {
                            let messages = "";
                            for (let error in response.errors) {
                                messages += (`
                        <div class="alert alert-danger">` + response.errors[error][0] + `
                        </div>`);

                            }
                            $(".depc").append(messages);
                        }
                    }
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });
        return false;
    });
    $("#update_department").submit(function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        let form = $(this);
        let formData = new FormData(form[0]);
        fetch(form.attr('action'), {
            method: form.attr('method'),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'accept': 'application/json'
            },
            body: formData,
        })
            .then(response => response.json())
            .then(function (response) {
                console.log(response)
                if (response.success) {
                    $("body").removeClass("active");
                    window.location.href = response.url;
                } else {
                    $(".depu").html(' ');
                    if (response.errors != undefined) {
                        if (Object.keys(response.errors).length != 0) {
                            let messages = "";
                            for (let error in response.errors) {
                                messages += (`
                        <div class="alert alert-danger">` + response.errors[error][0] + `
                        </div>`);

                            }
                            $(".depu").append(messages);
                        }
                    }
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });
        return false;
    });
    $("#create_employee").submit(function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        let form = $(this);
        let formData = new FormData(form[0]);
        fetch(form.attr('action'), {
            method: form.attr('method'),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'accept': 'application/json'
            },
            body: formData,
        })
            .then(response => response.json())
            .then(function (response) {
                console.log(response)
                if (response.success) {
                    $("body").removeClass("active");
                    window.location.href = response.url;
                } else {
                    $(".depc").html(' ');
                    if (response.errors != undefined) {
                        if (Object.keys(response.errors).length != 0) {
                            let messages = "";
                            for (let error in response.errors) {
                                messages += (`
                        <div class="alert alert-danger">` + response.errors[error][0] + `
                        </div>`);

                            }
                            $(".depc").append(messages);
                        }
                    }
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });
        return false;
    });
});
